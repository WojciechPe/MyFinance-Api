package com.wp.myfinance.FinanceAcount.Service;

import com.wp.myfinance.Common.AppSettings;
import com.wp.myfinance.FinanceAcount.FinanceAccount;
import com.wp.myfinance.FinanceAcount.repository.FinanceAccountRepository;
import com.wp.myfinance.Transaction.model.Transaction;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Hooks;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.List;

@DataMongoTest
@RunWith(SpringRunner.class)
public class FinanceAccountServiceImplTest {


//    @Autowired
//    FinanceAccountService financeAccountService;
    @Autowired
    FinanceAccountRepository financeAccountRepository;
    @Mock
    AppSettings appSettings;
    List<Transaction> transactionsList = new ArrayList<>();
    private final FinanceAccount financeAccount = new FinanceAccount(transactionsList,"test finance Account","1234");
    private final FinanceAccount financeAccount2 = new FinanceAccount(transactionsList,"test finance Account2","1234");
    private final FinanceAccount financeAccount3 = new FinanceAccount(transactionsList,"test finance Account 3","1234");

    @Before
    public void enableFluxDebug() {
        Hooks.onOperatorDebug();
    }

    @Test
    public void testShouldBeConsistent() throws Exception {
        Assertions.assertThat(financeAccount.getName()).isEqualToIgnoringWhitespace("test finance Account");
        Assertions.assertThat(financeAccount.getUserId()).isEqualToIgnoringWhitespace("1234");
    }

    @Test
    public void testShouldStepVerifyFinanceAccounts() throws Exception {
        StepVerifier.create(Flux.just(financeAccount.getName(), financeAccount.getUserId()))
                .expectNext("test finance Account", "1234")
                .expectComplete()
                .verify();
    }


    @Test
    public void testShouldFetchByName() {
        org.springframework.cglib.core.KeyFactory kf;

        Publisher<FinanceAccount> setup =
                this.financeAccountRepository
                        .deleteAll()
                        .checkpoint("saveAllTeams")
                        .thenMany(this.financeAccountRepository.saveAll(Flux.just(this.financeAccount, this.financeAccount2)));

        Publisher<FinanceAccount> find = this.financeAccountRepository.findByName("test finance Account");
        Publisher<FinanceAccount> composite = Flux.from(setup).thenMany(find);
        StepVerifier.create(composite).expectNext(this.financeAccount).verifyComplete();
    }


//    @Test
//    public void testShouldFetchFavorites() {
//        Publisher<FinanceAccount> setup =
//                this.financeAccountRepository
//                        .deleteAll()
//                        .thenMany(this.financeAccountRepository.saveAll(Flux.just(this.financeAccount, this.financeAccount2)));
//
//        Publisher<FinanceAccount> find = this.financeAccountRepository.findAll();
//       // find.subscribe(System.out::println);
//        Publisher<FinanceAccount> composite = Flux
//                .from(setup)
//                .thenMany(find);
//
//        StepVerifier
//                .create(composite)
//                .expectNext(this.financeAccount, this.financeAccount2)
//                .verifyComplete();
//    }

//    @Test
//    public void testShouldFetchFavorites() {
//        Publisher<FinanceAccount> setup =
//                this.financeAccountRepository.deleteAll().thenMany(this.financeAccountRepository.saveAll(Flux.just(this.financeAccount, this.financeAccount2)));
//        Publisher<FinanceAccount> find = this.financeAccountRepository.findAll();
//        Publisher<FinanceAccount> composite = Flux.from(setup).thenMany(find);
//        StepVerifier.create(composite).expectNext(this.financeAccount2, this.financeAccount).verifyComplete();
//    }

    }

//    @Test
//    public void getAll()
//    {
//        Mockito.when(appSettings.getCurrentUser()).thenReturn(Optional.of(new User("1234")));
//     //   Mockito.when(new User().getId()).thenReturn("1234");
//
//        Flux<FinanceAccount> saved = financeAccountRepository.saveAll(Flux.just(new FinanceAccount(null,"test Account", appSettings.getCurrentUser().get().getId()),
//                                                                                new FinanceAccount(null,"test Account 2", appSettings.getCurrentUser().get().getId()),
//                                                                                new FinanceAccount(null,"test Account 3", appSettings.getCurrentUser().get().getId())));
//
//
//        Optional<User> currentUser = appSettings.getCurrentUser();
//        Flux<FinanceAccount> composite = financeAccountService.getAllUserFinanceAccounts();
//
//        Predicate<FinanceAccount> match = financeAccount -> saved.any(saveItem -> saveItem.equals(financeAccount)).block();
//
//        StepVerifier
//                .create(composite)
//                .expectNextMatches(match)
//                .expectNextMatches(match)
//                .expectNextMatches(match)
//                .verifyComplete();
//    }
