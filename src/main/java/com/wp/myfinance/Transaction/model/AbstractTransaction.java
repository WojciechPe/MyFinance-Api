package com.wp.myfinance.Transaction.model;


import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.wp.myfinance.FinanceAcount.FinanceAccount;
import org.springframework.data.annotation.Id;


import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "classType")

@JsonSubTypes({
        @JsonSubTypes.Type(value = Transaction.class, name = "standard_transaction")
})

public abstract class AbstractTransaction {

    public enum TransactionType
    {
        expenditure,income
    }
    @Id
    private String id = String.valueOf(UUID.randomUUID());

    String title;

    TransactionCategory transactionCategory;
    TransactionType transactionType;
    String note;

    BigDecimal amount;
    TransactionType type;
    Date date;

    FinanceAccount financeAccountId;

    public AbstractTransaction() {
    }

    public AbstractTransaction(String id, String title, TransactionCategory transactionCategory, TransactionType transactionType,
                               String note, BigDecimal amount, TransactionType type, Date date, FinanceAccount financeAccountId) {
        this.id = id;
        this.title = title;
        this.transactionCategory = transactionCategory;
        this.transactionType = transactionType;
        this.note = note;
        this.amount = amount;
        this.type = type;
        this.date = date;
        this.financeAccountId = financeAccountId;
    }

    public AbstractTransaction(String title, TransactionCategory transactionCategory, TransactionType transactionType,
                               String note, BigDecimal amount, TransactionType type, Date date, FinanceAccount financeAccountId) {
        this.title = title;
        this.transactionCategory = transactionCategory;
        this.transactionType = transactionType;
        this.note = note;
        this.amount = amount;
        this.type = type;
        this.date = date;
        this.financeAccountId = financeAccountId;
    }

    public FinanceAccount getFinanceAccountId() {
        return financeAccountId;
    }

    public void setFinanceAccountId(FinanceAccount financeAccountId) {
        this.financeAccountId = financeAccountId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public TransactionCategory getTransactionCategory() {
        return transactionCategory;
    }

    public void setTransactionCategory(TransactionCategory transactionCategory) {
        this.transactionCategory = transactionCategory;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
