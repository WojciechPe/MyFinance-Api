package com.wp.myfinance.Transaction.model;


import com.fasterxml.jackson.annotation.JsonTypeName;
import com.wp.myfinance.FinanceAcount.FinanceAccount;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;


@Document(collection = "transaction")
@JsonTypeName("standard_transaction")
public class Transaction  {

    public Transaction(String title, TransactionCategory transactionCategory, AbstractTransaction.TransactionType transactionType,
                       String note, BigDecimal amount, AbstractTransaction.TransactionType type, Date date) {

        this.title = title;
        this.transactionCategory = transactionCategory;
        this.transactionType = transactionType;
        this.note = note;
        this.amount = amount;
        this.type = type;
        this.date = date;

    }

    public enum TransactionType
    {
        expenditure,income
    }
    @Id
    private String id = String.valueOf(UUID.randomUUID());

    String title;

    TransactionCategory transactionCategory;
    AbstractTransaction.TransactionType transactionType;
    String note;

    BigDecimal amount;
    AbstractTransaction.TransactionType type;
    Date date;

  //  FinanceAccount financeAccountId;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public TransactionCategory getTransactionCategory() {
        return transactionCategory;
    }

    public void setTransactionCategory(TransactionCategory transactionCategory) {
        this.transactionCategory = transactionCategory;
    }

    public AbstractTransaction.TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(AbstractTransaction.TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public AbstractTransaction.TransactionType getType() {
        return type;
    }

    public void setType(AbstractTransaction.TransactionType type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }



    @Override
    public String toString() {
        return "Transaction{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", transactionCategory=" + transactionCategory +
                ", transactionType=" + transactionType +
                ", note='" + note + '\'' +
                ", amount=" + amount +
                ", type=" + type +
                ", date=" + date +
                ", financeAccountId="  +
                '}';
    }
}
