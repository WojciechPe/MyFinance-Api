package com.wp.myfinance.Transaction.Repository;


import com.wp.myfinance.Transaction.model.AbstractTransaction;
import com.wp.myfinance.Transaction.model.Transaction;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface TransactionRepository extends ReactiveCrudRepository<Transaction,String> {

  //  public abstract List<AbstractTransaction> findByDateBetweenAndFinanceAccountId(Date start, Date end, String financeAccountId);


}
