package com.wp.myfinance.Transaction.Service;

import com.wp.myfinance.Transaction.Repository.TransactionRepository;
import com.wp.myfinance.Transaction.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    TransactionRepository transactionRepository;
    @Override
    public void saveTransaction(Transaction transaction) {
        transactionRepository.save(transaction).subscribe();
    }

    @Override
    public void deleteTransaction(Transaction transaction) {
        transactionRepository.delete(transaction).subscribe();
    }

    @Override
    public Flux<Transaction> getTransactions() {
        return  transactionRepository.findAll();
    }

//    @Override
//    public Flux<Transaction> getPlannedTransactions(Date startDate, Date enDate) {
//        return null;
//    }

//    @Override
//    public List<Transaction> getPlannedTransactions() {
//        return null;
//    }

//    @Override
//    public List<Transaction> getPlannedTransactions() {
////        Date now = new Date();
////        Date tomorow    = new Date()
////           return transactionRepository.findByDateBetweenAndFinanceAccount(now,now+) ;
//
//return transactionRepository.findAll();
//    }
}
