package com.wp.myfinance.Transaction.Service;


import com.wp.myfinance.Transaction.model.Transaction;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public interface TransactionService {

    void saveTransaction(Transaction transaction);
    void deleteTransaction(Transaction transaction);

    Flux<Transaction> getTransactions();
    //Flux<Transaction> getPlannedTransactions(Date startDate, Date enDate);

}
