package com.wp.myfinance.Transaction.Controller;

import com.wp.myfinance.Authorization.model.User;
import com.wp.myfinance.Common.AppSettings;
import com.wp.myfinance.FinanceAcount.FinanceAccount;
import com.wp.myfinance.FinanceAcount.repository.FinanceAccountRepository;
import com.wp.myfinance.Transaction.Repository.TransactionRepository;
import com.wp.myfinance.Transaction.Service.TransactionService;
import com.wp.myfinance.Transaction.model.AbstractTransaction;
import com.wp.myfinance.Transaction.model.Transaction;
import com.wp.myfinance.Transaction.model.TransactionCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/Transaction")
public class TransactionController {

    @Autowired
    TransactionService transactionService;
    @Autowired
    FinanceAccountRepository financeAccountRepository;
    @Autowired
    TransactionRepository transactionRepository;
    @Autowired
    AppSettings appSettings;
    @PostMapping("/addTransaction")
    public ResponseEntity addTransaction(@RequestBody Transaction transaction)
    {
        transactionService.saveTransaction(transaction);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/getTransactions")
    public Flux<Transaction>  getTransactions()
    {

       // return new ResponseEntity <List<AbstractTransaction>>>(transactionService.getTransactions(),HttpStatus.OK);
        Flux<Transaction> transactions = transactionService.getTransactions();
        //transactions.subscribe(transa -> System.out.println(transa.getTitle()));

        return transactions;
    }
//
//    @GetMapping("/getTransactions")
//    public ResponseEntity <List<AbstractTransaction>>  getPlanedTransactions()
//    {
//        // return new ResponseEntity <List<AbstractTransaction>>>(transactionService.getTransactions(),HttpStatus.OK);
//        return new ResponseEntity<List<AbstractTransaction>> (transactionService.getTransactions(),HttpStatus.OK);
//    }


    @GetMapping(value = "/TransactionTest",produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<Transaction> transactionTest()
    {
        // return new ResponseEntity <List<AbstractTransaction>>>(transactionService.getTransactions(),HttpStatus.OK);
        FinanceAccount financeAccount = new FinanceAccount();
        List<Transaction> transactions = new ArrayList<>();
        Date currentDate = new Date();

         User user =   appSettings.getCurrentUser().get();


        LocalDateTime localDateTime = currentDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        localDateTime = localDateTime.plusYears(1).plusMonths(1).plusDays(1);
        Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());

        localDateTime = localDateTime.plusYears(1).plusMonths(1).plusDays(1);

        Date currentDatePlus2day = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());

        localDateTime = localDateTime.plusYears(1).plusMonths(1).plusDays(1);
        Date currentDatePlus3day = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        user.addFinanceAccount(financeAccount);


        transactions.add(new Transaction("test1",new TransactionCategory(), AbstractTransaction.TransactionType.income,"test note",
        new BigDecimal(22), AbstractTransaction.TransactionType.income,currentDatePlusOneDay));

        transactions.add(new Transaction("test2",new TransactionCategory(), AbstractTransaction.TransactionType.income,"test note3",
                new BigDecimal(11), AbstractTransaction.TransactionType.income,currentDatePlus2day));

        transactions.add(new Transaction("test3",new TransactionCategory(), AbstractTransaction.TransactionType.income,"test note33",
                new BigDecimal(33), AbstractTransaction.TransactionType.income,currentDatePlus3day));

        transactions.forEach( tr ->
       {
           financeAccount.addTransaction(tr);
           transactionService.saveTransaction(tr);
       });
        Flux.fromIterable(transactions).subscribe(System.out::println);

        financeAccountRepository.save(financeAccount);
      Flux<Transaction> transactions1 = transactionService.getTransactions();
//      //List<Transaction>  all =  transactionRepository.findAll();
        transactions1.subscribe(transa -> System.out.println(transa.getTitle()));
        return (transactions1);
    }
    @GetMapping(value = "/allTransactions", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public  Flux<Transaction>  getAllTransactions()
    {
        Mono<Transaction> byId = null;//transactionRepository.findById("897b3bc8-3894-4216-8033-277a6ca9de1e");//.subscribe(System.out::println);  //findAll().delayElements(Duration.ofSeconds(5)).subscribe(System.out::println);
// return  transactionRepository.findAll().delayElements(Duration.ofSeconds(5));
        List<Transaction> transactions = new ArrayList<>();

        FinanceAccount financeAccount = new FinanceAccount();
        Date currentDate = new Date();

        User user =   appSettings.getCurrentUser().get();

        LocalDateTime localDateTime = currentDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        localDateTime = localDateTime.plusYears(1).plusMonths(1).plusDays(1);
        Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());

        localDateTime = localDateTime.plusYears(1).plusMonths(1).plusDays(1);

        Date currentDatePlus2day = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());

        localDateTime = localDateTime.plusYears(1).plusMonths(1).plusDays(1);
        Date currentDatePlus3day = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        user.addFinanceAccount(financeAccount);

        return   Flux.fromIterable(transactions);
    }



}
