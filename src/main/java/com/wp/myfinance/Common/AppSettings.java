package com.wp.myfinance.Common;

import com.wp.myfinance.Authorization.model.User;
import com.wp.myfinance.Authorization.security.UserPrincipal;
import com.wp.myfinance.Authorization.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AppSettings {

    private Optional<User> currentUser;

    @Autowired
    private UserService userService;

    public Optional<User> getCurrentUser() {
        Optional<User> currentUser = userService
                .findById(((UserPrincipal) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal())
                .getId());
        return currentUser;
    }

    public Optional<User> getTestUser() {
        final Optional<User> user = Optional.of(new User("1234"));
        return user;
    }
}
