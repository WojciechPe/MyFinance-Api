package com.wp.myfinance.Authorization.model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wp.myfinance.FinanceAcount.FinanceAccount;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

//
//@Entity
//@Table(name = "users", uniqueConstraints = {
//        @UniqueConstraint(columnNames = "email")
//})
@Document(collection = "user")
public class User {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
    @Id
    private String id;
    //@Field
    private String name;
    //  @Email
  //  @Column(nullable = false)
    private String email;

    private String imageUrl;

   // @Column(nullable = false)
    private Boolean emailVerified = false;

    @DBRef
    List<FinanceAccount> financeAccounts;

    @JsonIgnore
    private String password;

   // @NotNull
   // @Enumerated(EnumType.STRING)
    private AuthProvider provider;

    private String providerId;

    public String getId() {
        return id;
    }

    public User() {
        financeAccounts = new ArrayList<>();
    }
    public User(String id) {
        this.id =id;
        financeAccounts = new ArrayList<>();
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Boolean getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(Boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public AuthProvider getProvider() {
        return provider;
    }

    public void setProvider(AuthProvider provider) {
        this.provider = provider;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public List<FinanceAccount> getFinanceAccounts() {
        return financeAccounts;
    }

    public void setFinanceAccounts(List<FinanceAccount> financeAccounts) {
        this.financeAccounts = financeAccounts;
    }

    public  void addFinanceAccount(FinanceAccount financeAccount)
    {
        this.financeAccounts.add(financeAccount);
        financeAccount.setUserId(this.id);
    }
}
