package com.wp.myfinance.FinanceAcount.Controlers;

import com.wp.myfinance.FinanceAcount.FinanceAccount;
import com.wp.myfinance.FinanceAcount.Service.FinanceAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/FinanceAccount")
public class FinanceAccountController {

    @Autowired
    FinanceAccountService financeAccountService;

    @GetMapping("/allUserFinanceAccounts")
    public Flux<FinanceAccount> getAllUserAccounts() {
        return financeAccountService.getAllUserFinanceAccounts();
    }
 @GetMapping("/testFinanceAccount")
 public Mono<ResponseEntity<FinanceAccount>> test() {
        FinanceAccount financeAccount = new FinanceAccount();
        financeAccount.setName("test:finance account 1");
     return financeAccountService.addFinanceAccount(financeAccount)
             .map(savedFinanceAccount -> ResponseEntity.ok(savedFinanceAccount));
 }
    @PostMapping("/addFinanceAccount")
    public Mono<ResponseEntity<FinanceAccount>> addFinanceAccount(@RequestBody FinanceAccount financeAccount) {
        return financeAccountService.addFinanceAccount(financeAccount)
                .map(savedFinanceAccount -> ResponseEntity.ok(savedFinanceAccount));
    }

    @PostMapping("/updateFinanceAccount")
    public Mono<ResponseEntity<FinanceAccount>> updateFinanceAccount(@RequestBody FinanceAccount financeAccount) {
        return financeAccountService.updateFinanceAccount(financeAccount)
                .map(updatedFinanceAccount -> ResponseEntity.ok(updatedFinanceAccount));
    }
}
