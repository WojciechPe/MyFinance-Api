package com.wp.myfinance.FinanceAcount.repository;

import com.wp.myfinance.FinanceAcount.FinanceAccount;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface FinanceAccountRepository extends ReactiveMongoRepository<FinanceAccount,String> {

    Mono<FinanceAccount> findById(String id);
    Flux<FinanceAccount> findByUserId(String userId);
    Mono<FinanceAccount> findByName(String name);

    //Flux<FinanceAccount> findAll(String userId);

}
