package com.wp.myfinance.FinanceAcount;

import com.wp.myfinance.Transaction.model.AbstractTransaction;
import com.wp.myfinance.Transaction.model.Transaction;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


@Data
public class FinanceAccount {

    @Id
    private String id = String.valueOf(UUID.randomUUID());
    @DBRef
    List<Transaction> transactionsList;
    String name;
    String userId;
    Date createDate;
    BigDecimal amount;
    BigDecimal startAmount;




    public FinanceAccount() {
        transactionsList = new ArrayList<>();
    }

    public FinanceAccount(List<Transaction> transactionsList, String name, String userId) {
        this.transactionsList = transactionsList;
        this.name = name;
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Transaction> getTransactionsList() {
        return transactionsList;
    }

    public void setTransactionsList(List<Transaction> transactionsList) {
        this.transactionsList = transactionsList;
    }

    public String getName() {
        return name;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addTransaction(Transaction transaction)
    {
        this.transactionsList.add(transaction);
      //  transaction.setFinanceAccountId(this);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
