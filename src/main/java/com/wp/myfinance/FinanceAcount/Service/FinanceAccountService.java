package com.wp.myfinance.FinanceAcount.Service;

import com.wp.myfinance.FinanceAcount.FinanceAccount;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public interface FinanceAccountService {

    Flux<FinanceAccount> getAllUserFinanceAccounts();
    Mono<FinanceAccount> addFinanceAccount(FinanceAccount financeAccount);
    Mono<FinanceAccount> updateFinanceAccount(FinanceAccount financeAccount);
}
