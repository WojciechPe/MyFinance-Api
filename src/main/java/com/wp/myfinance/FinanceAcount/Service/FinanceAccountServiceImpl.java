package com.wp.myfinance.FinanceAcount.Service;

import com.wp.myfinance.Common.AppSettings;
import com.wp.myfinance.FinanceAcount.FinanceAccount;
import com.wp.myfinance.FinanceAcount.repository.FinanceAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Date;

@Service
public class FinanceAccountServiceImpl implements FinanceAccountService {
    @Autowired
    FinanceAccountRepository financeAccountRepository;
    @Autowired
    AppSettings appSettings;

    @Override
    public Flux<FinanceAccount> getAllUserFinanceAccounts() {
        return financeAccountRepository.findByUserId(appSettings.getCurrentUser().get().getId());
    }

    @Override
    public Mono<FinanceAccount> addFinanceAccount(FinanceAccount financeAccount) {
        financeAccount.setUserId(appSettings.getCurrentUser().get().getId());
        financeAccount.setCreateDate(new Date());
        return financeAccountRepository.save(financeAccount);
    }
    @Override
    public Mono<FinanceAccount> updateFinanceAccount(FinanceAccount financeAccount) {
        return financeAccountRepository.save(financeAccount);
    }

}
