package com.wp.myfinance.apiTest;

import com.wp.myfinance.Transaction.model.Transaction;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/test")
public class ApiTester {



    @GetMapping("/checkGetTransactions")
    public void testTransactions( ) {

        WebClient client = WebClient.create("http://localhost:8080");

        Flux<Transaction> employeeFlux = client.get()
                .uri("/Transaction/TransactionTest")
                .retrieve()
                .bodyToFlux(Transaction.class);

        employeeFlux.subscribe(System.out::println);
    }

}
